Real time path tracer with OpenCL.

Controls:
wasd: Move the camera.
Arrow keys: Rotate the camera.
r and f: Move the camera up and down as opposed to forward and backwards as w and s does.
+ and -: Change sphere selected.
Numpad (2,4,6,8,9,3): Move selected sphere.
7 and 1: Change sphere material.

The selected sphere is printed on console when hitting + or -.
The coordinates of the camera and spheres are printed in console if they change.
